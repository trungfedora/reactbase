import React, { Component } from 'react';
import {
    StyleSheet,
    Button,
    Image
} from 'react-native';

export default class ChatScreen extends Component {
    static navigationOptions = {
        tabBarLabel: 'Chat',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }}
                style={[styles.icon, { tintColor }]}
            />
        ),
    };

    render() {
        return (
            <Button
                onPress={() => this.props.navigation.navigate('Home')}
                title="Go back Home"
            />
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 50,
        height: 50,
    },
});

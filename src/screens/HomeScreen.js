import React, { Component } from 'react';
import {
    StyleSheet,
    Button,
    Image
} from 'react-native';

export default class HomeScreen extends Component {
    static navigationOptions = {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }}
                style={[styles.icon, { tintColor }]}
            />
        ),
    };

    render() {
        return (
            <Button
                onPress={() => this.props.navigation.navigate('Notifications')}
                title="Go to notifications"
            />
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 50,
        height: 50,
    },
});

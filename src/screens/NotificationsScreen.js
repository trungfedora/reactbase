import React, { Component } from 'react';
import {
    StyleSheet,
    Button,
    Image
} from 'react-native';

export default class NotificationsScreen extends Component {
    static navigationOptions = {
        tabBarLabel: 'Notifications',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }}
                style={[styles.icon, { tintColor }]}
            />
        ),
    };

    render() {
        return (
            <Button
                onPress={() => this.props.navigation.goBack()}
                title="Go back home"
            />
        );
    }
}
const styles = StyleSheet.create({
    icon: {
        width: 50,
        height: 50,
    },
});

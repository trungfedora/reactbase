/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
    TabNavigator
} from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import NotificationsScreen from './screens/NotificationsScreen';
import ChatScreen from './screens/ChatScreen';

const App = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
        },
        Notifications: {
            screen: NotificationsScreen,
        },
        Chat: {
            screen: ChatScreen,
        },
    },
    {
        tabBarPosition: 'bottom',
        animationEnabled: true,
        tabBarOptions: {
            activeTintColor: '#e91e63',
        }
    },
);
export default App;
